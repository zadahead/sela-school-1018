import { useState } from "react";

export const useCounter = (initialValue) => {
    //logic
    const [count, setCount] = useState(initialValue || 0);
    const handleAdd = () => {
        setCount(count + 1);
    }

    const rtn = {
        count: count,
        handleAdd: handleAdd
    }

    return rtn;
}