import { useState } from "react";

export const useColorSwitch = () => {
    //logic
    const [color, setColor] = useState('red');

    const handleSwitch = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }

    return {
        color,
        handleSwitch
    }
}
