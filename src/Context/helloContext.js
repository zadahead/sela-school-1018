import { createContext } from "react";

export const helloContext = createContext('');

const Provider = helloContext.Provider;

export const HelloProvider = ({ children }) => {
    const value = {
        text: "Hello Mosssss"
    }
    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}