import { createContext, useState } from "react";

export const countContext = createContext(0);

const { Provider } = countContext;

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const value = {
        count,
        handleAdd
    }
    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}