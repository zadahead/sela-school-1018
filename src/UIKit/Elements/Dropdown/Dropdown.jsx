import { useEffect } from 'react';
import { useState } from 'react';
import { Icon, Between } from 'UIKit';
import './Dropdown.css';

export const Dropdown = ({ list = [], selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(true);

    useEffect(() => {
        document.addEventListener('click', close);

        return () => {
            document.removeEventListener('click', close);
        }
    }, [])

    const handleToggle = (e) => {
        e.stopPropagation();

        setIsOpen(!isOpen);
        console.log('handleToggle')
    }

    const handleSelect = (id) => {
        console.log('handleSelect', id)
        onChange(id);
        setIsOpen(false);
    }

    const close = () => {
        setIsOpen(false);
        console.log('close')
    }

    const renderHeader = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.value;
            }
        }
        return 'Please Select..'
    }

    const getClass = (id) => {
        return id === selected ? 'selected' : '';
    }

    return (
        <div className='Dropdown'>
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h3>{renderHeader()}</h3>
                    <Icon i="chevron-down" />
                </Between>
            </div>
            {isOpen && (
                <div className='list'>
                    {list.map(i => (
                        <h4
                            key={i.id}
                            className={getClass(i.id)}
                            onClick={() => handleSelect(i.id)}
                        >
                            {i.value}
                        </h4>
                    ))}
                </div>
            )}
        </div>
    )
}