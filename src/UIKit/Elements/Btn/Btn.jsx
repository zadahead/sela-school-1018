import { Line, Icon } from 'UIKit';

import './Btn.css';

export const Btn = (props) => {
    const renderIcon = () => {
        if (props.i) {
            return <Icon i={props.i} />;
        }
        return null;
    }

    return (
        <button className={`Btn ${props.type}`} onClick={props.onClick}>
            <Line>
                {renderIcon()}
                {props.children}
            </Line>
        </button>
    )
}