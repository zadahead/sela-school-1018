import { forwardRef } from 'react';
import './Input.css';

export const Input = forwardRef((props, ref) => {

    const handleOnChange = (e) => {
        props.onChange(e.target.value);
    }

    return (
        <div className='Input'>
            <input
                value={props.value}
                onChange={handleOnChange}
                placeholder={props.p}
                type={props.type}
                ref={ref}
            />
        </div>
    )
})