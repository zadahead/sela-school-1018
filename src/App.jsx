import { countContext } from 'Context/countContext';
import { useContext } from 'react';
import { Route, Routes, NavLink } from 'react-router-dom';
import { Between, Grid, Line, Rows } from 'UIKit';
import { Custom } from 'Views/Custom';
import { DropdownView } from 'Views/DropdownView';
import { Inputs } from 'Views/Inputs';
import { LifeCycle } from 'Views/LifeCycle';
import { Lists } from 'Views/Lists';
import { RefWrap } from 'Views/Ref';
import { RestAPI } from 'Views/RestAPI';
import { About } from './Views/About';
import { BoxGame } from './Views/BoxGame';
import { Home } from './Views/Home';
import { States } from './Views/States';

export const App = () => {
    const value = useContext(countContext);

    return (
        <>
            <Grid>
                <div>
                    <Between>
                        <Line>
                            <NavLink to='/states'>states</NavLink>
                            <NavLink to='/inputs'>Inptus</NavLink>
                            <NavLink to='/lists'>Lists</NavLink>
                            <NavLink to='/cycle'>Cycle</NavLink>
                            <NavLink to='/rest'>Rest</NavLink>
                            <NavLink to='/ref'>Ref</NavLink>
                            <NavLink to='/dropdown'>Dropdown</NavLink>
                            <NavLink to='/custom'>Custom</NavLink>
                        </Line>
                        <h3>{value.count}</h3>
                    </Between>
                </div>
                <div>
                    <Rows>
                        <Routes>
                            <Route path='/home' element={<Home />} />
                            <Route path='/about' element={<About />} />
                            <Route path='/states' element={<States />} />
                            <Route path='/boxgame' element={<BoxGame />} />
                            <Route path='/inputs' element={<Inputs />} />
                            <Route path='/lists' element={<Lists />} />
                            <Route path='/cycle' element={<LifeCycle />} />
                            <Route path='/rest' element={<RestAPI />} />
                            <Route path='/ref' element={<RefWrap />} />
                            <Route path='/dropdown' element={<DropdownView />} />
                            <Route path='/custom' element={<Custom />} />
                            <Route path='/*' element={<h2>Default</h2>} />
                        </Routes>
                    </Rows>
                </div>
            </Grid>
        </>
    )
}

/*
    <Btn onClick /> => h1 / button


*/