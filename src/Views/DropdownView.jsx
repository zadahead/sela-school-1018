import { useState } from "react"
import { Dropdown, Rows } from "UIKit"

const list = [
    { id: 1, value: 'item 1' },
    { id: 2, value: 'item 2' },
    { id: 3, value: 'item 3' },
    { id: 4, value: 'item 4' }
]
export const DropdownView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <Rows>
            <h1>Drop down</h1>
            <Dropdown list={list} selected={selected} onChange={setSelected} />
        </Rows>
    )
}