import { Effect } from "Components/Effect"
import { NewCounter } from "Components/NewCounter"
import { Toggler } from "Components/Toggler"
import { Rows } from "UIKit"

export const LifeCycle = () => {
    return (
        <Rows>
            <h1>Life Cycle - useEffect</h1>
            <Toggler>
                <Effect />
            </Toggler>
        </Rows>
    )
}