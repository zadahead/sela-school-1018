import { useEffect, useRef, useState } from "react";
import { Btn, Input, Rows } from "UIKit"

export const Ref = () => {


    const [count, setCount] = useState(0);
    const inputRef = useRef();



    useEffect(() => {
        setTimeout(() => {
            inputRef.current.style.backgroundColor = 'red'
        }, 3000);
    }, [])


    return (
        <Rows>
            <h3>UseRef</h3>
            <Input ref={inputRef} />
            <Btn onClick={() => setCount(count + 1)}>Add</Btn>
        </Rows>
    )
}

export const RefWrap = () => {
    return (
        <Rows>
            <Ref />
            <Ref />
        </Rows>
    )
}