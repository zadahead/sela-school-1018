
import { CounterPro } from "Components/Custom/CounterPro"
import { Rows } from "UIKit"

export const Custom = () => {
    return (
        <Rows>
            <h3>Custom Hooks</h3>
            <CounterPro />
        </Rows>
    )
}