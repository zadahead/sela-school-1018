import { useState } from "react"
import { Btn, Input, Rows } from "UIKit";

export const Inputs = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = () => {
        console.log('username =>', username);
        console.log('password =>', password);
    }

    return (
        <Rows>
            <h1>Login</h1>
            <Input value={username} onChange={setUsername} p="Username" />
            <Input value={password} onChange={setPassword} p="Password" />
            <Btn onClick={handleLogin}>Login</Btn>
        </Rows>
    )
}