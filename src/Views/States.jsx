import { Counter } from "Components/Counter";
import { Toggler } from "Components/Toggler";
import { Rows } from "UIKit";
import { LightSwitch } from "../Components/LightSwitch";

export const States = () => {

    return (
        <Rows>
            <h1>States</h1>

            <Toggler >
                <Counter />
            </Toggler>
        </Rows>
    )
}