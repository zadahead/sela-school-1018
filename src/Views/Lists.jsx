import { User } from "Components/User"
import { useState } from "react"
import { Btn, Input, Line, Rows } from "UIKit"

const list = [
    { id: 1, name: 'Mosh', age: 35 },
    { id: 2, name: 'David', age: 25 },
    { id: 3, name: 'Other', age: 45 }
]

/*
    <User /> => ${id}: name: ${name}, age: (${age});
*/

export const Lists = () => {
    const [name, setName] = useState('');
    const [age, setAge] = useState('');
    const [users, setUsers] = useState(list);
    // console.log('post add', users);

    const handleAdd = () => {
        users.push({
            id: users.length + 1, name: name, age: age
        })
        //console.log('pre add', users);
        setUsers([...users])
    }

    const renderList = () => {
        return users.map((user) => {
            return <User key={user.id} {...user} />
        })
    }
    return (
        <Rows>
            <h1>Lists</h1>
            <Line>
                <Input value={name} onChange={setName} p="name" />
                <Input value={age} onChange={setAge} p="age" />
                <Btn onClick={handleAdd}>Add User</Btn>
            </Line>
            {renderList()}
        </Rows>
    )
}