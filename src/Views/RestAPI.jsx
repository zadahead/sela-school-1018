import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";

import Loader from 'Images/11.svg';
import { Btn, Line, Rows } from "UIKit";
import { useContext } from "react";
import { helloContext } from "Context/helloContext";
import { countContext } from "Context/countContext";

/*
    const {isloading, list } = useFetch('...')
*/

const useFetch = (path) => {
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const [data, setData] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        setError('');

        axios.get(`https://jsonplaceholder.typicode.com${path}`)
            .then(resp => {
                setTimeout(() => {
                    setData(resp.data)
                    setIsLoading(false);
                }, 500);
            }).catch(err => {
                setIsLoading(false);
                setError(err.message);
            })
    }, [path])

    return [
        isLoading,
        error,
        data,
        setData
    ]
}

export const RestAPI = () => {
    const value = useContext(helloContext);
    const countValue = useContext(countContext);
    const [path, setPath] = useState('/todos');

    const [isLoading, error, list, setList] = useFetch(path);

    const toggleComplete = (item) => {
        item.completed = !item.completed;
        console.log('toggleComplete', item.completed)
        setList([...list]);
    }


    const renderList = () => {
        if (error) {
            const errCss = {
                color: 'red'
            }
            return <h3 style={errCss}>{error}</h3>
        }

        if (isLoading) {
            const loaderCss = {
                width: '50px'
            }
            return <img src={Loader} alt="loader" style={loaderCss} />
        }


        return list.map(item => {
            const itemCss = {
                color: item.completed ? '#ddd' : '#333',
                textDecoration: item.completed ? 'line-through' : 'none'
            }
            return (
                <h4
                    onClick={() => toggleComplete(item)}
                    key={item.id}
                    style={itemCss}
                >
                    {path === '/todos' ? item.title : item.email}
                </h4>
            )
        })
    }

    return (
        <div>
            <Rows>
                <Line>
                    <h3>{countValue.count}</h3>
                    <h1>RestAPI</h1>
                    <h4>{value.text}</h4>
                </Line>
                <Line>
                    <Btn onClick={() => setPath('/users')}>Users</Btn>
                    <Btn onClick={() => setPath('/todos')}>Todos</Btn>
                    <Btn onClick={countValue.handleAdd}>Add Global Count</Btn>
                </Line>
                <div>
                    {renderList()}
                </div>
            </Rows>
        </div>
    )
}