import { useState } from "react"
import { Btn, Line, Rows } from "UIKit"

export const BoxGame = () => {

    const [y, setY] = useState(0);
    const [x, setX] = useState(0);

    const jump = 30;

    const styleCss = {
        backgroundColor: 'red',
        width: '50px',
        height: '50px',
        transform: `translate(${x}px, ${y}px)`,
        transition: '0.3s transform'
    }

    return (
        <Rows>
            <h2>Box Game</h2>
            <Line>
                <Btn onClick={() => setY(y - jump)}>Up</Btn>
                <Btn onClick={() => setY(y + jump)}>Down</Btn>
                <Btn onClick={() => setX(x - jump)}>Left</Btn>
                <Btn onClick={() => setX(x + jump)}>Right</Btn>
            </Line>
            <div style={styleCss}></div>
        </Rows>
    )
}