import { useState } from "react"
import { Btn, Rows } from "UIKit"

export const Toggler = (props) => {
    const [isShow, setIsShow] = useState(true);

    const handleSwitch = () => {
        setIsShow(!isShow);
    }

    const renderComponent = () => {
        if (isShow) {
            return props.children;
        }
    }
    return (
        <Rows>
            <h2>Toggler</h2>
            <Btn onClick={handleSwitch}>Toggler</Btn>
            <div>
                {renderComponent()}
            </div>
        </Rows>
    )
}