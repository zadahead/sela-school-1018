import { useColorSwitch } from "Hooks/useColorSwitch";
import { Btn, Line, Rows } from "UIKit"


export const ColorSwitcher = () => {
    const { color, handleSwitch } = useColorSwitch();

    //render
    const boxCss = {
        width: '50px',
        height: '50px',
        backgroundColor: color
    }

    return (
        <Rows>
            <h3>Color Switcher</h3>
            <Line>
                <div style={boxCss}></div>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>
        </Rows>
    )
}