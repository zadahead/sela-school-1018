import { useColorSwitch } from "Hooks/useColorSwitch"
import { useCounter } from "Hooks/useCounter";
import { Btn, Line, Rows } from "UIKit"

export const CounterPro = () => {
    const { color, handleSwitch } = useColorSwitch();
    const { count, handleAdd } = useCounter();

    const styleCss = {
        color
    }
    return (
        <Rows>
            <h3>Counter Pro</h3>
            <Line>
                <h4 style={styleCss}>Count, {count}</h4>
                <Btn onClick={handleSwitch}>Switch</Btn>
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </Rows>
    )
}