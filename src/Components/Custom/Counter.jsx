import { useCounter } from "Hooks/useCounter";
import { Btn, Line } from "UIKit"

/*
    1) create full running component
    2) separate between logic and render
    3) create a function start with 'use...'
    4) cut/paste logic to the 'use' function
    5) see what values the original component needs for render, 
        and return it from the 'use' function
    6) use the 'use' function
    7) export 'use' function from /Hooks folder 
        and share it with other components.
*/



export const Counter = () => {
    const rtn = useCounter();

    //render
    return (
        <div>
            <Line>
                <h3>Count, {rtn.count}</h3>
                <Btn onClick={rtn.handleAdd}>Add</Btn>
            </Line>
        </div>
    )
}