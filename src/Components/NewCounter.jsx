import { useEffect, useState } from "react"
import { Btn, Line, Rows } from "UIKit"

import Loader from 'Images/11.svg';

const list = [
    { id: 1, name: 'mosh' },
    { id: 2, name: 'ruth' }
]

export const NewCounter = () => {
    const [count1, setCount1] = useState(0);
    const [count2, setCount2] = useState(0);
    const [users, setUsers] = useState(null);

    useEffect(() => {
        console.log('rendered');
        setTimeout(() => {
            setUsers(list);
        }, 500);

        document.body.addEventListener('click', handleBodyClick)

        return () => {
            console.log('before unload');
            document.body.removeEventListener('click', handleBodyClick);
        }

    }, [])

    useEffect(() => {
        //console.log('mounted + updated', count);
    })

    useEffect(() => {
        console.log('users updated => ', users);
    }, [users])

    useEffect(() => {
        handleAdd2();
    }, [count1])

    const handleBodyClick = () => {
        console.log('handleBodyClick')
    }

    const handleAdd = () => {
        setCount1(count1 + 1);
    }

    const handleAdd2 = () => {
        setCount2(count2 + 1);
    }

    const renderUsers = () => {
        if (!users) {
            const loaderCss = {
                width: '50px'
            }
            return <img src={Loader} alt="loader" style={loaderCss} />;
        }

        return users.map(u => {
            return <h4 key={u.id}>{u.name}</h4>
        })
    }

    return (
        <Rows>
            <Line>
                <h3>Count1, {count1}</h3>
                <h3>Count2, {count2}</h3>
            </Line>
            <Line>
                <Btn onClick={handleAdd}>Add1</Btn>
                <Btn onClick={handleAdd2}>Add2</Btn>
            </Line>

            {renderUsers()}
        </Rows>
    )
}

