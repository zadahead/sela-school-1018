import { useState } from "react";
import { useEffect } from "react"
import { Btn, Rows } from "UIKit";

const list = [
    1, 2, 3, 4, 5
]

export const Effect = () => {
    const [state, setState] = useState(null);


    useEffect(() => {
        setTimeout(() => {
            setState(list);
        }, 2000);
    }, [])

    const logThis = async () => {
        console.log('aaaaa');

        await waitForPromise();

        console.log('ffff');

    }

    const waitFor = (callback) => {
        setTimeout(() => {
            console.log('ddddd');
            callback()
        }, 3000);
    }

    const waitForPromise = () => {
        return new Promise((callback) => {
            setTimeout(() => {
                console.log('ddddd');
                callback()
            }, 3000);
        })
    }


    return (
        <Rows>
            <h3>List:</h3>
            <Btn onClick={logThis}>Action</Btn>
            {!state && <h3>loading...</h3>}
            {state && (
                list.map(i => <h4 key={i}>{i}</h4>)
            )}
        </Rows>
    )
}