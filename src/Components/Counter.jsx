import { useState } from "react";
import { Btn, Icon, Line } from "UIKit";

export const Counter = () => {
    console.log('render');

    const [count, setCount] = useState(10);

    const handleAdd = () => {
        console.log('set state')
        setCount(count + 1);
    }

    const styleCss = {
        color: 'blue'
    }

    const renderIcon = () => {
        if (count < 15) {
            return <Icon i="heart" />
        }
        return null;
    }

    return (
        <Line>
            {renderIcon()}
            <h2 style={styleCss}>Count, {count}</h2>
            <Btn onClick={handleAdd}>Add</Btn>
        </Line>
    )
}