import { useState } from "react"
import { Btn, Line } from "UIKit"

export const ColorSwitcher = () => {
    const [color, setColor] = useState('red');
    const [count, setCount] = useState(0);

    console.log('color', color);

    const handleSwitch = () => {
        setColor(color === 'blue' ? 'red' : 'blue');
        setCount(0);
    }

    const handleAdd = () => {
        setCount(count + 1);
    }

    const styleCss = {
        color
    }

    return (
        <Line>
            <h2 style={styleCss}>Color Switcher, {count}</h2>
            <Btn onClick={handleSwitch}>Switch</Btn>
            <Btn onClick={handleAdd}>Add</Btn>
        </Line>
    )
}