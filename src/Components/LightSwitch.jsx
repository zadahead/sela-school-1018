import { useState } from "react"
import { Line } from "UIKit"

import Light from '../Images/lightbulb_on.png';
import Switch from '../Images/lightSwitch.jpg';

export const LightSwitch = () => {
    const [isOn, setIsOn] = useState(true);


    const handleSwitch = () => {
        setIsOn(!isOn);
    }

    const styleCss = {
        width: '150px',
        filter: `grayscale(${isOn ? '0' : '100'}%)`
    }

    const divStyle = {
        width: '100px',
        height: '138px',
        backgroundImage: `url(${Switch})`,
        backgroundPosition: `${isOn ? '-30' : '-119'}px -21px`,
        transform: 'rotate(180deg)'
    }

    return (
        <div>
            <Line>
                <img style={styleCss} src={Light} alt="light on" />
                <div style={divStyle} onClick={handleSwitch}></div>
            </Line>
        </div>
    )
}