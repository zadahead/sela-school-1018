import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';
import { App } from './App';

import { BrowserRouter } from 'react-router-dom';
import { HelloProvider } from 'Context/helloContext';
import { CountProvider } from 'Context/countContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <CountProvider>
    <HelloProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </HelloProvider>
  </CountProvider>
);

